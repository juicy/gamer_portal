$.fn.profile = () ->
	btn = this.find '.add_social_button'
	form = this.find 'form.social_form'
	to_copy = this.find '.to_copy .b_add_social'
	rm = this.find '.b_add_social button.red1'
	$(rm).click ->
		$(this).parent().remove()
		false
	$(btn).click ->
		$(to_copy).clone().appendTo(form)
		$(form).find('.b_add_social').last().find('button.red1').click ->
			$(this).parent().remove()
			false
		false
	
$.fn.b_posts = () ->
	box = this
	a = this.find 'a[data-target]'
	boxs = this.find 'div[data-id]'
	$(a).click ->
		$(a).removeClass 'active'
		$(this).addClass 'active'
		$(boxs).removeClass 'active'
		$(box).find("[data-id='#{$(this).attr('data-target')}']").addClass 'active'
		false
popups = () ->
  dp = '[data-role="popup"]'
  pops = $(dp)
  pops_open = $('[data-role="popup_open"]')
  pops_close = $('[data-role="popup_close"]')
  wind = '[data-role="popup_window"]'

  $(document).keyup((e) ->
    if e.keyCode == 27 && $(".open#{dp}").length > 0
      close(".open#{dp}")
  )

  open = (pop) ->
    if $(".open#{dp}").length > 0
      close($(".open#{dp}"))
    window.location.hash = pop.replace('#', '')
    $(pop).css('display', 'block')
    $(pop).addClass('open')
    $('body').css('overflow', 'hidden')

  close = (pop) ->
    history.pushState('', document.title, window.location.pathname);
    $(pop).css('display', 'none')
    $(pop).removeClass('open')
    $('body').css('overflow', 'auto')

  $('body').click ->
    if $(".open#{dp}").length > 0
      close(".open#{dp}")

  $(pops).each ->

    id = $(this).attr('id')

    hash = window.location.hash.replace('#', '')
    if hash != '' && hash == id
      open("##{id}")

    $(this).find(wind).click((e) ->
      e.stopPropagation();
    )

  $(pops_open).each ->
    $(this).click ->
      target = $(this).attr('data-target')
      if $(target).length > 0
        open(target)
      false

  $(pops_close).each ->
    $(this).click ->
      target = $(this).parents(dp)
      if $(target).length > 0
        close(target)
      false 

$.fn.b_subscribe = () ->
	$(this).each ->
		$(this).find('button[data-toggle-text]').click ->
			text = $(this).attr('data-toggle-text')
			clas = $(this).attr('data-toggle-class')
			$(this).attr('data-toggle-text', $(this).html())
			$(this).attr('data-toggle-class', $(this).attr('class'))
			$(this).html(text)
			$(this).attr('class', clas)
			false

$.fn.b_tags = () ->
	as = $(this).find('.d>ul>li>a')
	as.click ->
		$(this).parent().toggleClass 'open'
		false

$.fn.b_head_tabs_box = () ->
	$(this).each ->
		ls = $(this).find '.h ul a[data-elems]'
		box = this
		$(ls).click ->
			$(ls).removeClass 'active'
			$(this).addClass 'active'
			$(box).find('.d').removeClass 'active'
			$(box).find(".d[data-elems=#{$(this).attr('data-elems')}]").addClass 'active'
			false

$.fn.form_header_fun = () ->
	form = this
	btn = this.find 'button'
	input = this.find 'input'
	$(btn).click ->
		unless $(form).hasClass 'open'
			$(form).addClass 'open'
			$(input).focus()
			false
	$(form).click (e) ->
		e.stopPropagation()
	$(document).click ->
		$(form).removeClass 'open'

carusel = (block, in_window, width, left, right, wrap, time, points, napr) ->

	th = 0
	max = $(block).find("ul li").length - in_window
	hover = false

	auto = ->
		to th + 1	unless hover
		setTimeout (->
			auto()
		), time

	to = (num) ->

		num = max if num < 0
		num = 0 if num > max

		if napr is "top"
			$(block).find(wrap).animate
				"margin-top": num * -1 * width
			, 500, ->
				th = num

		if napr is "left"
			$(block).find(wrap).animate
				"margin-left": num * -1 * width
			, 500, ->
				th = num

		if points
			$(block).find("#{points} a").removeClass("active").addClass("passive")
			$(block).find("#{points} a:eq(#{num})").removeClass("passive").addClass("active")

	setTimeout (->
		auto()
	), time

	$(block).hover (->
		hover = true
	), ->
		hover = false

	$(block).find(left).click ->
		to th - 1
		false

	$(block).find(right).click ->
		to th + 1
		false
	
	if points
		$(block).find("#{points} a").click ->
			n = $(this).prevAll().length
			to n
			false


$.fn.comments = () ->
	box = this
	add = this.find 'a.add'
	$(add).click ->
		text = $(this).attr('data-toggle-text')
		$(this).attr('data-toggle-text', $(this).html())
		$(this).html(text)
		form = $(box).find('div.add_comment')
		comment = $(this).parents('.b_comment')
		$(box).find('div.add_comment').remove()
		if $(this).hasClass 'remove'
			$(form).appendTo($(box).find('.form_spot'))
		else
			$(form).insertAfter(this)
		$(this).toggleClass 'remove'
		false

$('.b_comments').comments()
$('.b_subscribe').b_subscribe()
$('article.friends').b_subscribe()
$('.b_user_page').b_subscribe()
$('select').customSelect()
$('.b_tags').b_tags()
$('header .search form').form_header_fun()
$('.b_head_box').b_head_tabs_box()
$('span.sub').click ->
	$(this).toggleClass('cl')
#popups()
$('.b_posts').b_posts()
carusel '.b_carusel', 1, 1160, '', '', 'ul', 3000, '.points', 'left'
$('.carusel776').each ->
	carusel this, 1, 776, 'a.left', 'a.right', '.dts', 99999, false, 'left'
$('.carusel423').each ->
	carusel this, 1, 423, 'a.left', 'a.right', '.dts', 99999, false, 'left'
$('.carusel239').each ->
	carusel this, 1, 239, 'a.left', 'a.right', '.dts', 99999, false, 'left'
$('.b_film_carusel').each ->
	carusel this, 3, 250, 'a.left', 'a.right', '.wrap', 99999, false, 'left'
$('#da-slider').cslider()
$('article').profile()