var carusel, popups;

$.fn.profile = function() {
  var btn, form, rm, to_copy;
  btn = this.find('.add_social_button');
  form = this.find('form.social_form');
  to_copy = this.find('.to_copy .b_add_social');
  rm = this.find('.b_add_social button.red1');
  $(rm).click(function() {
    $(this).parent().remove();
    return false;
  });
  return $(btn).click(function() {
    $(to_copy).clone().appendTo(form);
    $(form).find('.b_add_social').last().find('button.red1').click(function() {
      $(this).parent().remove();
      return false;
    });
    return false;
  });
};

$.fn.b_posts = function() {
  var a, box, boxs;
  box = this;
  a = this.find('a[data-target]');
  boxs = this.find('div[data-id]');
  return $(a).click(function() {
    $(a).removeClass('active');
    $(this).addClass('active');
    $(boxs).removeClass('active');
    $(box).find("[data-id='" + ($(this).attr('data-target')) + "']").addClass('active');
    return false;
  });
};

popups = function() {
  var close, dp, open, pops, pops_close, pops_open, wind;
  dp = '[data-role="popup"]';
  pops = $(dp);
  pops_open = $('[data-role="popup_open"]');
  pops_close = $('[data-role="popup_close"]');
  wind = '[data-role="popup_window"]';
  $(document).keyup(function(e) {
    if (e.keyCode === 27 && $(".open" + dp).length > 0) {
      return close(".open" + dp);
    }
  });
  open = function(pop) {
    if ($(".open" + dp).length > 0) {
      close($(".open" + dp));
    }
    window.location.hash = pop.replace('#', '');
    $(pop).css('display', 'block');
    $(pop).addClass('open');
    return $('body').css('overflow', 'hidden');
  };
  close = function(pop) {
    history.pushState('', document.title, window.location.pathname);
    $(pop).css('display', 'none');
    $(pop).removeClass('open');
    return $('body').css('overflow', 'auto');
  };
  $('body').click(function() {
    if ($(".open" + dp).length > 0) {
      return close(".open" + dp);
    }
  });
  $(pops).each(function() {
    var hash, id;
    id = $(this).attr('id');
    hash = window.location.hash.replace('#', '');
    if (hash !== '' && hash === id) {
      open("#" + id);
    }
    return $(this).find(wind).click(function(e) {
      return e.stopPropagation();
    });
  });
  $(pops_open).each(function() {
    return $(this).click(function() {
      var target;
      target = $(this).attr('data-target');
      if ($(target).length > 0) {
        open(target);
      }
      return false;
    });
  });
  return $(pops_close).each(function() {
    return $(this).click(function() {
      var target;
      target = $(this).parents(dp);
      if ($(target).length > 0) {
        close(target);
      }
      return false;
    });
  });
};

$.fn.b_subscribe = function() {
  return $(this).each(function() {
    return $(this).find('button[data-toggle-text]').click(function() {
      var clas, text;
      text = $(this).attr('data-toggle-text');
      clas = $(this).attr('data-toggle-class');
      $(this).attr('data-toggle-text', $(this).html());
      $(this).attr('data-toggle-class', $(this).attr('class'));
      $(this).html(text);
      $(this).attr('class', clas);
      return false;
    });
  });
};

$.fn.b_tags = function() {
  var as;
  as = $(this).find('.d>ul>li>a');
  return as.click(function() {
    $(this).parent().toggleClass('open');
    return false;
  });
};

$.fn.b_head_tabs_box = function() {
  return $(this).each(function() {
    var box, ls;
    ls = $(this).find('.h ul a[data-elems]');
    box = this;
    return $(ls).click(function() {
      $(ls).removeClass('active');
      $(this).addClass('active');
      $(box).find('.d').removeClass('active');
      $(box).find(".d[data-elems=" + ($(this).attr('data-elems')) + "]").addClass('active');
      return false;
    });
  });
};

$.fn.form_header_fun = function() {
  var btn, form, input;
  form = this;
  btn = this.find('button');
  input = this.find('input');
  $(btn).click(function() {
    if (!$(form).hasClass('open')) {
      $(form).addClass('open');
      $(input).focus();
      return false;
    }
  });
  $(form).click(function(e) {
    return e.stopPropagation();
  });
  return $(document).click(function() {
    return $(form).removeClass('open');
  });
};

carusel = function(block, in_window, width, left, right, wrap, time, points, napr) {
  var auto, hover, max, th, to;
  th = 0;
  max = $(block).find("ul li").length - in_window;
  hover = false;
  auto = function() {
    if (!hover) {
      to(th + 1);
    }
    return setTimeout((function() {
      return auto();
    }), time);
  };
  to = function(num) {
    if (num < 0) {
      num = max;
    }
    if (num > max) {
      num = 0;
    }
    if (napr === "top") {
      $(block).find(wrap).animate({
        "margin-top": num * -1 * width
      }, 500, function() {
        return th = num;
      });
    }
    if (napr === "left") {
      $(block).find(wrap).animate({
        "margin-left": num * -1 * width
      }, 500, function() {
        return th = num;
      });
    }
    if (points) {
      $(block).find("" + points + " a").removeClass("active").addClass("passive");
      return $(block).find("" + points + " a:eq(" + num + ")").removeClass("passive").addClass("active");
    }
  };
  setTimeout((function() {
    return auto();
  }), time);
  $(block).hover((function() {
    return hover = true;
  }), function() {
    return hover = false;
  });
  $(block).find(left).click(function() {
    to(th - 1);
    return false;
  });
  $(block).find(right).click(function() {
    to(th + 1);
    return false;
  });
  if (points) {
    return $(block).find("" + points + " a").click(function() {
      var n;
      n = $(this).prevAll().length;
      to(n);
      return false;
    });
  }
};

$.fn.comments = function() {
  var add, box;
  box = this;
  add = this.find('a.add');
  return $(add).click(function() {
    var comment, form, text;
    text = $(this).attr('data-toggle-text');
    $(this).attr('data-toggle-text', $(this).html());
    $(this).html(text);
    form = $(box).find('div.add_comment');
    comment = $(this).parents('.b_comment');
    $(box).find('div.add_comment').remove();
    if ($(this).hasClass('remove')) {
      $(form).appendTo($(box).find('.form_spot'));
    } else {
      $(form).insertAfter(this);
    }
    $(this).toggleClass('remove');
    return false;
  });
};

$('.b_comments').comments();

$('.b_subscribe').b_subscribe();

$('article.friends').b_subscribe();

$('.b_user_page').b_subscribe();

$('select').customSelect();

$('.b_tags').b_tags();

$('header .search form').form_header_fun();

$('.b_head_box').b_head_tabs_box();

$('span.sub').click(function() {
  return $(this).toggleClass('cl');
});

$('.b_posts').b_posts();

carusel('.b_carusel', 1, 1160, '', '', 'ul', 3000, '.points', 'left');

$('.carusel776').each(function() {
  return carusel(this, 1, 776, 'a.left', 'a.right', '.dts', 99999, false, 'left');
});

$('.carusel423').each(function() {
  return carusel(this, 1, 423, 'a.left', 'a.right', '.dts', 99999, false, 'left');
});

$('.carusel239').each(function() {
  return carusel(this, 1, 239, 'a.left', 'a.right', '.dts', 99999, false, 'left');
});

$('.b_film_carusel').each(function() {
  return carusel(this, 3, 250, 'a.left', 'a.right', '.wrap', 99999, false, 'left');
});

$('#da-slider').cslider();

$('article').profile();
